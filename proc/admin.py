from django.contrib import admin

# Register your models here.
from proc.models import Proc, ProfileProc

admin.site.register(Proc)
admin.site.register(ProfileProc)
