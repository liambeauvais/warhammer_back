from django.db import models

# Create your models here.
from profil.models import Profile


class Proc(models.Model):

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Procs"

    name = models.CharField(null=False, blank=False)


class ProfileProc(models.Model):

    def __str__(self):
        return f"Profile_proc with id {self.id}"

    roll = models.PositiveIntegerField(null=False, blank=False)
    value = models.PositiveIntegerField(null=False, blank=False)
    profile = models.ForeignKey(Profile, related_name="procs", on_delete=models.CASCADE, null=False)
    proc = models.ForeignKey(Proc, on_delete=models.CASCADE, null=False)
