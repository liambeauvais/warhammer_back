from rest_framework import routers

from proc.views import ProcViewSet,ProfileProcViewSet

router = routers.SimpleRouter()
router.register("proc", ProcViewSet, basename="proc")
router.register("profile_proc", ProfileProcViewSet, basename="profile_proc")

urlpatterns = [

]

urlpatterns += router.urls
