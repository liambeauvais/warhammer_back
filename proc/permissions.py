from rest_framework import permissions

from proc.models import ProfileProc


class ProcPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_staff or request.user.is_superuser:
            return True
        if request.method in permissions.SAFE_METHODS:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser or request.user.is_staff:
            return True
        return False


class ProfileProcPermissions(permissions.BasePermission):
    def has_object_permission(self, request, view, obj: ProfileProc):
        if request.user.is_superuser or request.user.is_staff:
            return True

        if obj.profile.army.user.id == request.user.id:
            return True
        return False
