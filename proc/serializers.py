from rest_framework import serializers

from proc.models import ProfileProc, Proc


class ProcSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proc
        fields = '__all__'


class ProfileProcSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProfileProc
        fields = '__all__'
