from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from proc.models import ProfileProc, Proc
from proc.permissions import ProcPermissions, ProfileProcPermissions
from proc.serializers import ProfileProcSerializer, ProcSerializer


class ProfileProcViewSet(viewsets.ModelViewSet):
    serializer_class = ProfileProcSerializer
    permission_classes = [IsAuthenticated & ProfileProcPermissions]

    def get_queryset(self):
        if self.request.user.is_superuser or self.request.user.is_staff:
            return ProfileProc.objects.all()
        return ProfileProc.objects.filter(profile__army__user_id=self.request.user.id)


class ProcViewSet(viewsets.ModelViewSet):
    queryset = Proc.objects.all()
    serializer_class = ProcSerializer
    permission_classes = [IsAuthenticated & ProcPermissions]

