import django_filters
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from algorithm.algorithm.algorithm import Algorithm
from algorithm.models.Procs import Procs
from algorithm.models.AlgoProfile import AlgoProfile
from algorithm.models.defender import Defender
from profil.models import Profile
from profil.permissions import ProfilePermissions
from profil.serializers import ProfileSerializer


class ProfileViewSet(viewsets.ModelViewSet):
    serializer_class = ProfileSerializer
    permission_classes = [IsAuthenticated & ProfilePermissions]

    def get_queryset(self):
        if self.request.user.is_superuser or self.request.user.is_staff:
            return Profile.objects.all()
        return Profile.objects.filter(army__user_id=self.request.user.id)

    @action(detail=False, methods=['post'], permission_classes=[])
    def get_statistics(self, request):
        profile_data = request.data['profile']
        defender_data = request.data['defender']
        profile = AlgoProfile(
            model_number=profile_data['model_number'],
            model_attack_number=profile_data['model_attack'],
            has_chief=profile_data['has_chief'],
            to_hit=profile_data['to_hit'],
            to_wound=profile_data['to_wound'],
            rend=profile_data['rend'],
            dmg_type=profile_data['dmg_type'],
            hit_re_roll=profile_data['hit_re_roll'],
            wound_re_roll=profile_data['wound_re_roll'],
        )
        # request.data['procs'] is returned as
        # {
        # hit_proc:{'name':<proc_name>,'value':<value>,'roll':<roll>},
        # wound_proc:{'name':<proc_name>,'value':<value>,'roll':<roll>},
        # }
        procs: dict = request.data["profile"]['procs']
        profile.procs = [Procs(
            name=proc['name'],
            roll=proc['roll'],
            value=proc['value'],
        ) for key, proc in procs.items() if
            proc['name'] is not None and proc['roll'] is not None and proc['value'] is not None
        ]

        wide_effects = request.data['wide_effects']
        hit_modifier = wide_effects['hit_modifier']
        wound_modifier = wide_effects['wound_modifier']
        if hit_modifier is not None:
            profile.to_hit += int(hit_modifier)
        if wound_modifier is not None:
            profile.to_wound += int(wound_modifier)

        defender = Defender(
            **defender_data
        )

        algorithm = Algorithm(
            defender=defender,
            profile=profile
        )

        statistics = algorithm.launch_n_algorithms(5000)
        return Response(data=statistics)
