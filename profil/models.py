from django.db import models

# Create your models here.
from army.models import Army


class Profile(models.Model):
    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Profiles"

    name = models.CharField(null=False, blank=False, default="profile")
    model_number = models.PositiveIntegerField(null=False, blank=False)
    model_attack = models.CharField(null=False, blank=False,default="1")
    has_chief = models.BooleanField(null=False, blank=False, default=False)
    to_hit = models.PositiveIntegerField(null=False, blank=False)
    to_wound = models.PositiveIntegerField(null=False, blank=False)
    rend = models.PositiveIntegerField(null=False, blank=False)
    dmg_type = models.CharField(null=False, blank=False,default="1")
    hit_re_roll = models.CharField(null=False, blank=False)
    wound_re_roll = models.CharField(null=False, blank=False)
    army = models.ForeignKey(Army, related_name="profiles", null=False, blank=False, on_delete=models.CASCADE)
