from rest_framework import serializers

from proc.serializers import ProfileProcSerializer
from profil.models import Profile


class ProfileSerializer(serializers.ModelSerializer):
    procs = ProfileProcSerializer(many=True, read_only=True)

    class Meta:
        model = Profile
        fields = '__all__'
