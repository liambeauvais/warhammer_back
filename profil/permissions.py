from rest_framework import permissions

from profil.models import Profile


class ProfilePermissions(permissions.BasePermission):
    def has_object_permission(self, request, view, obj: Profile):
        if request.user.is_superuser or request.user.is_staff:
            return True

        if obj.army.user == request.user:
            return True

        if obj.army.user != request.user:
            return False

        return False
