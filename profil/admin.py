from django.contrib import admin

# Register your models here.
from profil.models import Profile

admin.site.register(Profile)
