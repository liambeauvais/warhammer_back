from rest_framework import routers

from profil.views import ProfileViewSet

router = routers.SimpleRouter()
router.register("profile", ProfileViewSet, basename="Profile")

urlpatterns = [

]

urlpatterns += router.urls
