from algorithm.models.AlgoProfile import AlgoProfile

profile = AlgoProfile(model_number=1, has_chief=False)


class TestProfileAttackAmount:

    def test_attack_flat(self):
        profile.model_attack_number = "4"
        assert profile.get_roll_amount() == 4

    def test_attack_dices(self):
        profile.model_attack_number = "2D3"
        assert profile.get_roll_amount() in [i for i in range(2, 7)]

    def test_attack_flat_and_chief(self):
        profile.model_attack_number = "4"
        profile.has_chief = True
        assert profile.get_roll_amount() == 5

    def test_dmgs_flat(self):
        profile.dmg_type = '4'
        assert profile.get_unit_dmgs() == 4

    def test_dmgs_dices(self):
        profile.dmg_type = '2D6'
        assert profile.get_unit_dmgs() in [i for i in range(2, 13)]
