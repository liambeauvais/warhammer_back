from algorithm.algorithm.dice import Dice
from algorithm.models.AlgoProfile import AlgoProfile

dice = Dice()


class TestDice:

    def test_flat_value(self):
        dice.value_type = "4"
        assert dice.get_value() == 4

        dice.value_type = "20"
        assert dice.get_value() == 20

    def test_dice_value(self):
        dice.value_type = "D6"
        assert dice.get_value() in [1,2,3,4,5,6]

    def test_dice_value_and_addition(self):
        dice.value_type = "D3+2"
        assert dice.get_value() in [3,4,5]
