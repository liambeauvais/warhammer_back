import random


class Dice:
    def __init__(self, value_type: str = "D6", damages: int = 0, is_attack_sup: bool = False, has_passed: bool = False):
        self.value_type = value_type
        self.value = self.get_value()
        self.damages = damages
        self.is_attack_sup = is_attack_sup
        self.has_passed = has_passed

    def get_value(self) -> int:
        """
        Rolls a die, depending on value_type parameter, and returns his value. \n
        :return: The value of the dice
        """
        if "D" not in self.value_type:
            return int(self.value_type)
        if "+" in self.value_type:
            specificities = self.value_type.split('+')
        else:
            specificities = [self.value_type, "0"]
        # Get the value of the dice (D6 or D3 actually) then add the bonus (+1 for example)
        value = random.randint(1, int(specificities[0][1])) + int(specificities[1])

        return value
