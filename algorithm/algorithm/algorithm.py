from algorithm.algorithm.dice import Dice
from algorithm.models.AlgoProfile import AlgoProfile
from algorithm.models.defender import Defender
from algorithm.models.dmg_count import Damages


def get_final_data(iterations: list[Damages], profile: AlgoProfile, defender: Defender) -> dict:
    """
    Makes the average of all fields of all iterations. \n
    :param iterations: list of iterations as Damages
    :return: an object Damages containing all averages
    """
    final_data = Damages(
        hit_number=sum([dmg.hit_number for dmg in iterations]) / len(iterations),
        damages_taken=sum([dmg.damages_taken for dmg in iterations]) / len(iterations),
        MW_number=sum([dmg.MW_number for dmg in iterations]) / len(iterations),
        wound_number=sum([dmg.wound_number for dmg in iterations]) / len(iterations),
        damages_after_ward=sum([dmg.damages_after_ward for dmg in iterations]) / len(iterations),
        saves=sum([dmg.saves for dmg in iterations]) / len(iterations),
        ward_saves=sum([dmg.ward_saves for dmg in iterations]) / len(iterations),
    )
    return {
        "hit_number": final_data.hit_number,
        "damages_taken": final_data.damages_taken + final_data.MW_number,
        "MW_number": final_data.MW_number,
        "wound_number": final_data.wound_number,
        "damages_after_ward": final_data.damages_after_ward,
        "saves": final_data.saves,
        "ward_saves": final_data.ward_saves,
        "profile": {
            "model_number": profile.model_number,
            "attacks_per_model": profile.model_attack_number,
            "has_chief": profile.has_chief,
            "rend": profile.rend,
            "to_hit": profile.to_hit,
            "to_wound": profile.to_wound,
            "hit_reroll": profile.hit_re_roll,
            "wound_reroll": profile.wound_re_roll,
            "defender": {
                "save": defender.save,
                "ward": defender.ward,
                "re_roll": defender.re_roll,
                "ethereal": defender.ethereal
            }
        }
    }


def get_stats_percentage(stats: dict, iterations: int) -> list[dict]:
    """
    Receive some stats from iterations and returns them as percentage (float). \n
    :param stats: number of time the values got touched
    :param iterations: number of iterations
    :return: Returns the `stats` values as percentage
    """
    percentage_result = [
        {"name": "hits", "data": []},
        {"name": "wounds", "data": []},
        {"name": "dmgs_B4_save", "data": []},
        {"name": "dmgs_after_ward", "data": []},
    ]

    for key in stats["hits"]:
        percentage_result[0]['data'].append(stats["hits"][key] * 100 / iterations)
    for key in stats["wounds"]:
        percentage_result[1]['data'].append(stats["wounds"][key] * 100 / iterations)
    for key in stats["dmgs_B4_save"]:
        percentage_result[2]['data'].append(stats["dmgs_B4_save"][key] * 100 / iterations)
    for key in stats["dmgs_after_ward"]:
        percentage_result[3]['data'].append(stats["dmgs_after_ward"][key] * 100 / iterations)

    return percentage_result


class Algorithm:
    def __init__(self, defender: Defender, profile: AlgoProfile):
        self.defender = defender
        self.profile = profile
        self.rend = self.profile.rend
        self.attack_sup = 0
        self.wound_dices = []
        self.hit_dices = []

    def reset(self):
        self.rend = self.profile.rend
        self.attack_sup = 0
        self.wound_dices = []
        self.hit_dices = []

    def check_proc(self, dice: Dice, proc_name_to_check: str, target: dict) -> bool:
        """
        Checks if Profile has the proc, then check if the dice procs. \n
        :param dice: dice rolled
        :param proc_name_to_check: attribute to check
        :param target: target to implement if the dice procs (if there is only the field attribute, the target is self)
        :return: a boolean to indicate if Profile has the proc or not
        """
        # If the profile has the proc
        if self.profile.procs is not None:
            proc = None
            for profile_proc in self.profile.procs:
                if profile_proc.name == proc_name_to_check:
                    proc = profile_proc
                    break

            if proc is not None and dice.value >= int(proc.roll):
                # Getting the target to implement
                targeted = self if 'target' not in target else target['target']
                # Adding value of proc to the target
                setattr(
                    targeted,
                    target['attribute'],
                    getattr(targeted, target['attribute']) + int(proc.value)
                )
                return True

        return False

    def check_procs(self, dice: Dice, procs_to_check: list, attack_sup_procs_to_check: list):
        """
        Check all the possible procs of a Profile. \n
        :param dice: Dice to check
        :param procs_to_check:
        :param attack_sup_procs_to_check:
        :return:
        """

        if self.profile.procs is not None:
            hit_proc = None
            for proc in self.profile.procs:
                # non generic proc
                if proc.name == 'hit_on_hit':
                    hit_proc = proc
                    break

            # check of hit_proc
            if hit_proc is not None and dice.value >= hit_proc.roll:
                for hit in range(hit_proc.value):
                    self.hit_dices.append(Dice())

            for proc_to_check in procs_to_check:
                self.check_proc(dice=dice, proc_name_to_check=proc_to_check[0], target=proc_to_check[1])
            if not dice.is_attack_sup:
                for proc_to_check in attack_sup_procs_to_check:
                    self.check_proc(dice=dice, proc_name_to_check=proc_to_check[0], target=proc_to_check[1])

    def roll_hit_dice(self, damages: Damages, is_attack_sup: bool = False) -> Dice:
        """
        Rolls a hit dice and deal with procs. \n
        :param is_attack_sup: to know if this is an additional attack
        :param damages: Damages object of the actual iteration
        :param is_attack_sup: is rolled Dice an additional attack?
        :return: True to stop or False to re-roll
        """
        dice = Dice(is_attack_sup=is_attack_sup)
        # list of procs, index 0 is the profile.proc to check, index 1 the target to implement
        procs_to_check = [
            ['DMG_on_hit', {'target': damages, 'attribute': 'damages_taken'}],
            ['additional_MW_on_hit', {'target': damages, 'attribute': 'MW_number'}],
            ['rend_on_hit', {'attribute': 'rend'}],
            ['additional_DMG_on_hit', {'target': dice, 'attribute': 'damages'}],
        ]
        attack_procs_to_check = [
            ['attacks_on_hit', {'attribute': 'attack_sup'}],
        ]

        # proc MW_on_hit, stop the roll and MW
        if self.check_proc(dice, 'MW_on_hit', {'target': damages, 'attribute': 'MW_number'}):
            dice.has_passed = True
            return dice

        self.check_procs(dice, procs_to_check, attack_procs_to_check)

        if dice.value >= self.profile.to_hit:
            self.hit_dices.append(dice)
            dice.has_passed = True
            return dice

        return dice

    def roll_wound_dice(self, damages: Damages, dice: Dice):
        """
        Rolls a wound dice and deal with procs. \n
        :param damages: Damages object of the actual iteration
        :param dice: Actual rolled dice
        :return: dice value or stop
        """

        dice.has_passed = False
        # re-rolling the value of the dice instead of rolling a new one, to keep data on it
        # (e.g: additional dmg on it)
        dice.value = dice.get_value()
        if self.check_proc(dice, 'MW_on_wound', {'target': damages, 'attribute': 'MW_number'}):
            dice.has_passed = True

        # list of procs, index 0 is the profile.attribute to check, index 1 the target to implement
        procs_to_check = [
            ['DMG_on_wound', {'target': damages, 'attribute': 'damages_taken'}],
            ['additional_MW_on_wound', {'target': damages, 'attribute': 'MW_number'}],
            ['rend_on_wound', {'attribute': 'rend'}],
            ['additional_DMG_on_wound', {'target': dice, 'attribute': 'damages'}],
        ]
        attack_procs_to_check = [
            ['attacks_on_wound', {'attribute': 'attack_sup'}],
        ]

        self.check_procs(dice, procs_to_check, attack_procs_to_check)
        if dice.value >= self.profile.to_wound:
            dice.has_passed = True
            self.wound_dices.append(dice)

    def roll_save_dice(self, dice: Dice, damages: Damages, is_rerolled: bool = False):
        """
        Rolls a save dice and deal with procs. \n
        :param is_rerolled: the dice has already been rolled, and it failed
        :param dice: The Dice object we try to save
        :param damages: Damages object of the actual iteration
        :return: dice value or stop
        """
        dice.has_passed = False
        dice.value = dice.get_value()

        # save is updated if defender is not ethereal, then checks if defender saves the attack
        if dice.value < self.defender.save + (
                self.profile.rend if (self.profile.rend is not None and not self.defender.ethereal) else 0):
            if self.defender.re_roll and not is_rerolled:
                self.roll_save_dice(dice=dice, damages=damages, is_rerolled=True)
            else:
                damages.damages_taken += self.profile.get_unit_dmgs() + dice.damages
        else:
            damages.saves += 1

    def roll_ward_dice(self, damages: Damages):
        """
        Rolls a ward dice and deal with procs. \n
        :param damages: Damages object of the actual iteration
        """
        ## ward can be None and we need to add a damage for stats return
        if self.defender.ward is None:
            damages.damages_after_ward += 1
            return
        dice_value = Dice().value
        if dice_value < self.defender.ward:
            damages.damages_after_ward += 1
        else:
            damages.ward_saves += 1

    def hit_phase(self, is_attack_sup: bool, damages: Damages):
        attack_number = self.attack_sup if is_attack_sup else self.profile.get_roll_amount()
        if not is_attack_sup:
            damages.init_attacks = attack_number
        for roll in range(attack_number):
            dice = self.roll_hit_dice(damages, is_attack_sup=is_attack_sup)

            if not dice.has_passed and dice.value <= self.profile.hit_re_roll:
                self.roll_hit_dice(damages, is_attack_sup=is_attack_sup)

    def wound_phase(self, damages: Damages):
        for dice in self.hit_dices:
            self.roll_wound_dice(damages, dice)
            if not dice.has_passed and dice.value <= self.profile.wound_re_roll:
                self.roll_wound_dice(damages, dice)

    def do_algorithm(self) -> Damages:
        """
        Run algorithm to simulate a die roll. \n
        :return: an object Damages showing all important data from the roll
        """
        damages = Damages()

        # Profile part
        # we reset variables to implement
        self.reset()

        # first hit and wound phase
        self.hit_phase(is_attack_sup=False, damages=damages)
        self.wound_phase(damages=damages)

        # attack_sup hit and wound phase, we reset hit dices after adding the amount of dices in Damages object
        damages.hit_number += len(self.hit_dices)
        self.hit_dices = []
        self.hit_phase(is_attack_sup=True, damages=damages)
        self.wound_phase(damages=damages)
        damages.hit_number += len(self.hit_dices)
        damages.wound_number += len(self.wound_dices)

        # defender part
        for dice in self.wound_dices:
            self.roll_save_dice(dice=dice, damages=damages)

        for roll in range(damages.damages_taken + damages.MW_number):
            self.roll_ward_dice(damages)

        return damages

    def launch_n_algorithms(self, iteration_number) -> dict:
        """
        launch do_algorithm() as many as iteration number. \n
        :param iteration_number: asked number of iterations
        :return: a tuple returning the final data as Damages and a dict returning stats as percentage
        """
        iterations: list[Damages] = []
        stats = {
            "hits": {index: 0 for index in range(0, 10)},
            "wounds": {index: 0 for index in range(0, 10)},
            "dmgs_B4_save": {index: 0 for index in range(0, 10)},
            "dmgs_after_ward": {index: 0 for index in range(0, 10)},
        }
        for i in range(iteration_number):
            result = self.do_algorithm()
            iterations.append(result)

            for index in range(0, result.wound_number + 1):
                if index in stats["wounds"]:
                    stats["wounds"][index] += 1
                else:
                    stats["wounds"][index] = 1

            for index in range(0, result.hit_number + 1):
                if index in stats["hits"]:
                    stats["hits"][index] += 1
                else:
                    stats["hits"][index] = 1

            for index in range(0, result.damages_taken + result.MW_number + 1):
                if index in stats["dmgs_B4_save"]:
                    stats["dmgs_B4_save"][index] += 1
                else:
                    stats["dmgs_B4_save"][index] = 1

            for index in range(0, result.damages_after_ward + 1):
                if index in stats["dmgs_after_ward"]:
                    stats["dmgs_after_ward"][index] += 1
                else:
                    stats["dmgs_after_ward"][index] = 1

        final_data = get_final_data(iterations, profile=self.profile, defender=self.defender)

        stats = get_stats_percentage(stats, iteration_number)

        return {'final_data': final_data, 'stats': stats}
