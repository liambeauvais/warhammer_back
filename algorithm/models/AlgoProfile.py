from algorithm.algorithm.dice import Dice
from algorithm.models.Procs import Procs


class AlgoProfile:
    def __init__(self,
                 rend: str,
                 model_number: int = 5,
                 model_attack_number: str = "2",
                 has_chief: bool = True,
                 to_hit: int = 3,
                 to_wound: int = 4,
                 procs: list[Procs] = [],
                 dmg_type: str = "D6",
                 hit_re_roll: str = None,
                 wound_re_roll: str = None,
                 ):
        self.model_number = int(model_number)
        self.model_attack_number = model_attack_number
        self.has_chief = has_chief
        self.to_hit = int(to_hit)
        self.to_wound = int(to_wound)
        self.rend = int(rend) if (rend is not None and rend.isdigit()) else None
        self.dmg_type = dmg_type
        self.hit_re_roll = self.get_re_roll_value(hit_re_roll)
        self.wound_re_roll = self.get_re_roll_value(wound_re_roll)
        self.procs = procs

    def get_re_roll_value(self, re_roll: str) -> int:
        """
        Defines what is the hit re-roll value.\n
        :param re_roll: String defining the re-roll value( re-roll on 1, or all fails for example)
        :return: the re-roll value (0 for no re-roll)
        """
        if re_roll == "reroll 1":
            return 1
        elif re_roll == "reroll failed":
            return self.to_hit - 1
        else:
            return 0

    def get_roll_amount(self) -> int:
        """
        Gets the attacks type of Profile, then returns the amount of attack to roll.
        :return: Number of attacks
        """
        attack_amount = 0
        for i in range(self.model_number):
            multiplicator = 1
            dice_type = self.model_attack_number
            if "D" in self.model_attack_number:
                dice_type = f"D{self.model_attack_number.split('D')[1]}"
                multiplicator = int(self.model_attack_number.split('D')[0])
            for i in range(multiplicator):
                dice = Dice(value_type=dice_type)
                attack_amount += dice.value
        if self.has_chief:
            attack_amount += 1

        return attack_amount

    def get_unit_dmgs(self):
        dmg_amount = 0

        multiplicator = 1
        dice_type = self.dmg_type
        if "D" in self.dmg_type:
            dice_type = f"D{self.dmg_type.split('D')[1]}"
            multiplicator = int(self.dmg_type.split('D')[0])
        for i in range(multiplicator):
            dice = Dice(value_type=dice_type)
            dmg_amount += dice.value

        return dmg_amount
