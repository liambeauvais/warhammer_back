class Defender:
    def __init__(self, re_roll: str = "reroll1", save: str = 3, ward: str = 7, ethereal: bool = False):
        self.re_roll = re_roll
        self.ethereal = ethereal
        self.save = int(save)
        self.ward = int(ward) if (ward is not None and ward.isdigit()) else None

    def __str__(self):
        return f"re-roll:{self.re_roll}, save:{self.save}, ward:{self.ward}, ethereal:{self.ethereal}"
