class Procs:
    def __init__(self,
                 name: str = None,
                 roll: int = None,
                 value: int = None
                 ):
        self.name = name
        self.roll = roll
        self.value = value

    def __str__(self):
        return f"name:{self.name},roll:{self.roll},value:{self.value}"
