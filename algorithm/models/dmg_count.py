class Damages:
    def __init__(self,
                 hit_number: int or float = 0,
                 damages_taken: int or float = 0,
                 MW_number: int or float = 0,
                 wound_number: int or float = 0,
                 damages_after_ward: int or float = 0,
                 saves: int or float = 0,
                 ward_saves: int or float = 0,
                 init_attacks: int or float = 0
                 ):
        self.hit_number = hit_number
        self.damages_taken = damages_taken
        self.MW_number = MW_number
        self.wound_number = wound_number
        self.damages_after_ward = damages_after_ward
        self.saves = saves
        self.ward_saves = ward_saves
        self.init_attacks = init_attacks

    def __str__(self):
        return f"hit_number:{self.hit_number}, dmg_taken:{self.damages_taken}, MW_number:{self.MW_number}," \
               f" wound_number:{self.wound_number},final_damages:{self.damages_after_ward},saves:{self.saves},ward_saves:{self.ward_saves}"
