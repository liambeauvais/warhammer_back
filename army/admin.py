from django.contrib import admin

# Register your models here.
from army.models import Army

admin.site.register(Army)
