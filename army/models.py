from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from faction.models import Faction


class Army(models.Model):
    class Meta:
        verbose_name_plural = "Armies"

    def __str__(self):
        return self.name

    name = models.CharField(null=False, blank=False)
    faction = models.ForeignKey(Faction, related_name="armies", null=True, on_delete=models.SET_NULL)
    user = models.ForeignKey(User, related_name="armies", null=False, on_delete=models.CASCADE)
