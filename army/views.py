# Create your views here.
from rest_framework import viewsets

from army.models import Army
from army.serializers import ArmySerializer


class ArmyViewSet(viewsets.ModelViewSet):
    serializer_class = ArmySerializer

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Army.objects.all()
        return Army.objects.filter(user_id=self.request.user.id)