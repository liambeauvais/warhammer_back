from rest_framework import routers

from army.views import ArmyViewSet

router = routers.SimpleRouter()
router.register("army", ArmyViewSet, basename="army")

urlpatterns = [

]

urlpatterns += router.urls
