from rest_framework import permissions

from army.models import Army


class ArmyPermissions(permissions.BasePermission):

    def has_object_permission(self, request, view, obj: Army):
        if obj.user == request.user or request.user.is_superuser or request.user.is_staff:
            return True
        return False
