from rest_framework import serializers

from army.models import Army
from profil.serializers import ProfileSerializer


class ArmySerializer(serializers.ModelSerializer):
    profiles = ProfileSerializer(many=True, read_only=True)

    class Meta:
        model = Army
        fields = "__all__"
