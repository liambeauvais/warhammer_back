from rest_framework import serializers

from faction.models import Faction


class FactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Faction
        fields = "__all__"
