from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets, permissions

from faction.models import Faction
from faction.serializers import FactionSerializer


class ReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.method in permissions.SAFE_METHODS


class FactionViewSet(viewsets.ModelViewSet):
    queryset = Faction.objects.all()
    serializer_class = FactionSerializer
    permission_classes = [permissions.IsAdminUser|ReadOnly]
