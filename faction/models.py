from django.db import models


class Faction(models.Model):

    def __str__(self):
        return self.name

    name = models.CharField(null=False, blank=False,default="faction")
    logo = models.FileField(null=True, upload_to="assets/")
