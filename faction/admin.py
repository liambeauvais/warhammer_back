from django.contrib import admin

# Register your models here.
from faction.models import Faction

admin.site.register(Faction)
