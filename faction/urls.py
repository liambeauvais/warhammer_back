from rest_framework import routers

from faction.views import FactionViewSet

router = routers.SimpleRouter()
router.register("faction", FactionViewSet, basename="faction")

urlpatterns = [

]

urlpatterns += router.urls
